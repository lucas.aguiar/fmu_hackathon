﻿using fmu_hackathon.Models.Exception;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;

namespace fmu_hackathon.Helpers
{
    public class Helper
    {
        public void ShowValidation(object obj)
        {
            var erros = GetValidationErros(obj);
            foreach (var error in erros)
                throw new ArgumentException(error.ErrorMessage);
        }

        public static IEnumerable<ValidationResult> GetValidationErros(object obj)
        {
            var resultadoValidacao = new List<ValidationResult>();

            var contexto = new ValidationContext(obj, null, null);
            Validator.TryValidateObject(obj, contexto, resultadoValidacao, true);

            return resultadoValidacao;
        }

        public WebExceptionResponse WebException(ArgumentException e)
        {

            return new WebExceptionResponse()
            {
                Success = false,
                Data = DefineDataArgumentException(e.Message)
            };

        }

        public WebExceptionResponse WebException(Exception e)
        {

            return new WebExceptionResponse()
            {
                Success = false,
                Data = DefineDataArgumentException(e.Message)
            };

        }

        public static Data DefineDataArgumentException(string mensage)
        {
            var data = new Data();

            data.Code = 998;
            data.Message = mensage;

            data.HttpStatusCode = HttpStatusCode.BadRequest;
            return data;
        }

        public WebExceptionResponse WebException(SqlException e)
        {

            return new WebExceptionResponse()
            {
                Success = false,
                Data = new Data()
                {
                    Code = e.Number,
                    HttpStatusCode = HttpStatusCode.InternalServerError,
                    Message = e.Message,
                    //Name = e.Message
                }
            };
        }

    }
}