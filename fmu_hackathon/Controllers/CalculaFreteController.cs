﻿using fmu_hackathon.Helpers;
using fmu_hackathon.Models.CalculaFrete;
using fmu_hackathon.Models.Exception;
using fmu_hackathon.Structure;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace fmu_hackathon.Controllers
{
    [RoutePrefix("api/v1/frete")]
    public class CalculaFreteController : ApiController
    {
        private readonly Helper _help = new Helper();
        private WebExceptionResponse webError = new WebExceptionResponse();
        private ExternalDall _dall = new ExternalDall();

        /// <summary>
        /// Endpoint para geração/cálculo do frete.
        /// </summary>
        /// <param name="request">Classe que fornece os dados para o cálculo.</param>
        [Route("calcula")]
        [HttpPost]
        public HttpResponseMessage Calcula([FromBody] RequestFrete request)
        {
            HttpResponseMessage response;
            try
            {
                _help.ShowValidation(request);

                var _ret = _dall.GetFreteValor(request);

                return Request.CreateResponse(HttpStatusCode.OK, _ret);
            }
            catch (SqlException ex)
            {
                webError = _help.WebException(ex);
                response = Request.CreateResponse(webError.Data.HttpStatusCode, webError);
            }
            catch (ArgumentException arg)
            {
                webError = _help.WebException(arg);
                response = Request.CreateResponse(webError.Data.HttpStatusCode, webError);
            }
            catch (Exception ex)
            {
                webError = _help.WebException(ex);
                response = Request.CreateResponse(webError.Data.HttpStatusCode, webError);
            }

            return response;
        }
    }
}
