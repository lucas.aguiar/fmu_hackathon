﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace fmu_hackathon.Models.GoogleApi
{
    public class Element
    {
        public Distance distance { get; set; }
        public Duration duration { get; set; }
        public string status { get; set; }
    }
}