﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace fmu_hackathon.Models.Exception
{
    public class Data
    {
        public string Message { get; set; }
        public int Code { get; set; }
        public HttpStatusCode HttpStatusCode { get; set; }
    }
}