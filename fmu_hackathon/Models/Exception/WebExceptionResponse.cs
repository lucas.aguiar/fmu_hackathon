﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace fmu_hackathon.Models.Exception
{
    public class WebExceptionResponse
    {
        public bool Success { get; set; }
        public Data Data { get; set; }
    }
}