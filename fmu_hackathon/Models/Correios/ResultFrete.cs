﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace fmu_hackathon.Models.Correios
{
    public class ResultFrete
    {
        public Decimal Valor { get; set; }
        public Decimal ValorFrete { get; set; }
        public Decimal ValorTaxa { get; set; }
        public int PrazoDias { get; set; }
        public Decimal Total { get; set; }

    }
}