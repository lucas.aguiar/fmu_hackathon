﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace fmu_hackathon.Models.CalculaFrete
{
    public class RequestFrete
    {
        /// <summary>
        /// Cep de origem para o cálculo
        /// </summary>
        [Required(ErrorMessage = "O campo cep de origem é obrigatório", AllowEmptyStrings = false)]
        public string CepOrigem { get; set; }

        /// <summary>
        /// Cep do destino para o cálculo
        /// </summary>
        [Required(ErrorMessage = "O campo cep de destino é obrigatório", AllowEmptyStrings = false)]
        public string CepDestino { get; set; }

        /// <summary>
        /// Valor do produtoo
        /// </summary>
        [Required(ErrorMessage = "O campo valor é obrigatório", AllowEmptyStrings = false)]
        [Range(0.1, 9999999999999999.99)]
        public decimal Valor { get; set; }

        /// <summary>
        /// Altura do produto/mercadoria
        /// </summary>
        [Required(ErrorMessage = "A campo altura é obrigatório", AllowEmptyStrings = false)]
        [Range(0.1, 9999999999999999.99, ErrorMessage = "Prencha o campo obrigatório")]
        public decimal Altura { get; set; }

        /// <summary>
        /// Comprimento do produto/mercadoria
        /// </summary>
        [Required(ErrorMessage = "O campo comprimento é obrigatório", AllowEmptyStrings = false)]
        [Range(1, 9999999999999999.99, ErrorMessage = "Prencha o campo obrigatório")]
        public decimal Comprimento { get; set; }

        /// <summary>
        /// Largura do produto/mercadoria
        /// </summary>
        [Required(ErrorMessage = "O campo Largura é obrigatório", AllowEmptyStrings = false)]
        [Range(1, 9999999999999999.99, ErrorMessage = "Prencha o campo obrigatório")]
        public decimal Largura { get; set; }

        /// <summary>
        /// Diametro do produto/mercadoria
        /// </summary>
        //[Required(ErrorMessage = "O campo Diametro é obrigatório", AllowEmptyStrings = false)]
        //[Range(1, 9999999999999999.99, ErrorMessage = "Prencha o campo obrigatório")]
        public decimal Diametro { get; set; }

        /// <summary>
        /// Peso do produto/mercadoria
        /// </summary>
        [Required(ErrorMessage = "O campo peso obrigatório", AllowEmptyStrings = false)]
        [Range(0.1, 9999999999999999.99, ErrorMessage = "Prencha o campo obrigatório")]
        public decimal Peso { get; set; }

        /// <summary>
        /// Taxa aplicada ao valor do produto (%).
        /// </summary>
        public decimal TaxaServico { get; set; }

        /// <summary>
        /// Código do vigente. Padrão 04014 - SEDEX
        /// </summary>
        public string CodigoVigente { get; set; } = "04014";

    }
}