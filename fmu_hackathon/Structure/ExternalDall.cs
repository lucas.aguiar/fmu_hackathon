﻿using fmu_hackathon.correios;
using fmu_hackathon.Models.CalculaFrete;
using fmu_hackathon.Models.Correios;
using fmu_hackathon.Models.GoogleApi;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace fmu_hackathon.Structure
{
    public class ExternalDall
    {
        private readonly string _webAPIStr;
        private readonly string _token;
        private readonly string _accessKey;
        private readonly string _clientID;
        private RestClient client;

        public ResultFrete GetFreteValor(RequestFrete model)
        {
            var resultFrete = new ResultFrete();

            using (CalcPrecoPrazoWS _correios = new correios.CalcPrecoPrazoWS())
            {
                var _result = _correios.CalcPrecoPrazo(string.Empty, string.Empty, model.CodigoVigente, model.CepOrigem, model.CepDestino, model.Peso.ToString(), 1, model.Comprimento, model.Altura, model.Largura, model.Diametro, "N", 0m, "N");

                if (_result.Servicos.Where(x => x.MsgErro != string.Empty && x.MsgErro != null).Any())
                    throw new ArgumentException(_result.Servicos.Where(x => x.MsgErro != string.Empty && x.MsgErro != null).Select(x => x.MsgErro).FirstOrDefault());

                resultFrete.ValorFrete = _result.Servicos.Where(x => x.Erro == string.Empty || x.Erro == "0").Select(x => decimal.Parse(x.Valor)).FirstOrDefault();
                resultFrete.Valor = model.Valor;
                resultFrete.ValorTaxa = (model.TaxaServico / 100) * model.Valor;
                resultFrete.PrazoDias = _result.Servicos.Where(x => x.Erro == string.Empty || x.Erro == "0").Select(x => int.Parse(x.PrazoEntrega)).FirstOrDefault();
                resultFrete.Total = resultFrete.Valor + resultFrete.ValorFrete + resultFrete.ValorTaxa;
            }

            return resultFrete;

        }

    }
}